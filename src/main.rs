use std::{
    io::{BufWriter, Write},
    sync::{
        atomic::{AtomicBool, Ordering},
        Barrier,
    },
    time::Instant,
};

fn generate_primes_single_threaded(max_value: usize) -> Vec<usize> {
    let size = max_value + 1;
    let mut prime_map = vec![true; size];

    let to_sqrt = 2..(f64::sqrt(max_value as f64) as usize);

    for index in to_sqrt {
        if let Some(slot) = prime_map.get(index) {
            let slot: &bool = slot;

            if *slot {
                for composite in (index * 2..size).step_by(index) {
                    if let Some(slot) = prime_map.get_mut(composite) {
                        *slot = false;
                    }
                }
            }
        }
    }

    (0..size)
        .filter(|index| {
            if let Some(slot) = prime_map.get(*index) {
                let slot: &bool = slot;

                *slot
            } else {
                false
            }
        })
        .collect::<Vec<_>>()
}

fn generate_primes_multi_threaded(threads: usize, max_value: usize) -> Vec<usize> {
    let size = max_value + 1;
    let prime_map = (0..size).map(|_| AtomicBool::new(true)).collect::<Vec<_>>();

    let to_sqrt = (2..(f64::sqrt(max_value as f64) as usize)).collect::<Vec<usize>>();

    let barrier = Barrier::new(threads);

    std::thread::scope(|scope| {
        let mut handles = Vec::with_capacity(threads);

        for thread_num in 0..threads {
            let prime_map = &prime_map;
            let to_sqrt = &to_sqrt;
            let barrier = &barrier;

            handles.push(scope.spawn(move || {
                for index in to_sqrt.iter().skip(thread_num).step_by(threads) {
                    if let Some(slot) = prime_map.get(*index) {
                        let slot: &AtomicBool = slot;

                        if slot.load(Ordering::Relaxed) {
                            for composite in (index * 2..size).step_by(*index) {
                                if let Some(slot) = prime_map.get(composite) {
                                    slot.store(false, Ordering::Relaxed);
                                }
                            }
                        }
                    }
                }

                // Ensure all threads finish finding primes before building output
                barrier.wait();

                (thread_num..size)
                    .step_by(threads)
                    .filter(|index| {
                        if let Some(slot) = prime_map.get(*index) {
                            let slot: &AtomicBool = slot;

                            slot.load(Ordering::Relaxed)
                        } else {
                            false
                        }
                    })
                    .collect::<Vec<_>>()
            }));
        }

        handles
            .into_iter()
            .map(|handle| handle.join().expect("Thread completed"))
            .flatten()
            .collect::<Vec<usize>>()
    })
}

fn run_multi_threaded(max_prime: usize) -> Vec<usize> {
    let now = Instant::now();

    let threads: usize = std::thread::available_parallelism()
        .map(Into::into)
        .unwrap_or(1);

    println!(
        "{:?} - Generating primes up to {max_prime} with {threads} threads",
        now.elapsed()
    );

    let mut primes = generate_primes_multi_threaded(threads, max_prime);
    println!("{:?} - Generated {} primes", now.elapsed(), primes.len());
    primes.sort();
    println!("{:?} - Sorted", now.elapsed());
    primes
}

fn run_single_threaded(max_prime: usize) -> Vec<usize> {
    let now = Instant::now();
    println!(
        "{:?} - Generating primes up to {max_prime} with 1 thread",
        now.elapsed()
    );
    let primes = generate_primes_single_threaded(max_prime);
    println!("{:?} - Generated {} primes", now.elapsed(), primes.len());
    primes
}

fn print_primes(primes: Vec<usize>) {
    let buf = Vec::with_capacity(primes.len() * 8);
    let mut buf_writer = BufWriter::new(buf);
    for prime in primes {
        write!(buf_writer, "{prime}\n").expect("Writing succedes");
    }
    buf_writer.flush().expect("Flush succeeds");
    println!("Built output string");
    /*
    println!(
        "{}",
        String::from_utf8(buf_writer.into_inner().expect("Buffer is flushed"))
            .expect("Buf is valid")
    );
    */
}

fn main() {
    let max_prime = 3_000_000_000;

    print_primes(run_multi_threaded(max_prime));
    print_primes(run_single_threaded(max_prime));
}
