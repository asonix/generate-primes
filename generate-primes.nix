{ lib
, makeWrapper
, nixosTests
, protobuf
, rustPlatform
, stdenv
}:

rustPlatform.buildRustPackage {
  pname = "generate-primes";
  version = "0.1.0";
  src = ./.;
  cargoSha256 = "VuN7Hi9Ep4fE6WAOIccc2ZGOxZ0JMyVBE498lB6l54o=";

  nativeBuildInputs = [ ];

  passthru.tests = { inherit (nixosTests) generate-primes; };

  meta = with lib; {
    description = "A fast prime sieve";
    homepage = "https://git.asonix.dog/asonix/generate-primes";
    license = with licenses; [ agpl3Plus ];
  };
}
